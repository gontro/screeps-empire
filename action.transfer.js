module.exports = function(creep, job, target) {
    return creep.transfer(target, job.resource || RESOURCE_ENERGY);
};