var config = require('engine.config');
var log = require('engine.log');


module.exports = function(room, cache) {
    var runner = {

        initialize: function(force) {
            cache.add(room, 'room');

            if (room.memory.initialized && !force) {
                return false;
            }

            room.memory.initialized = true;
            room.memory.debug = !!room.memory.debug;
            room.memory.role = room.memory.role !== undefined ? room.memory.role : config.ROOM_STARTING_ROLE;
            room.memory.tier = room.memory.role !== undefined ? room.memory.role : config.ROOM_TIER_0_NAME;
            room.memory.manifest = [];
            room.memory.timers = { refresh: config.ROOM_TICK_RATE };
            return true;
        },

        timeout: function() {
            if (room.memory.timers.refresh === 0) {
                room.memory.timers.refresh = config.ROOM_TICK_RATE;
                return true;
            }
            room.memory.timers.refresh--;
            return false;
        },

        run: function() {
            // Ensure setup
            this.initialize();

            if (!room.memory.role) {
                return;
            }

            if (!this.timeout()) {
                return;
            }

            try {
                var roleRunner = require(config.ROOM_ROLE_PREFIX + room.memory.role);
            } catch(e) {
                log.room(room, "failure while trying to load role '" + config.ROOM_ROLE_PREFIX + room.memory.role + "'", true);
                return;
            }

            var tier;
            if (room.energyCapacityAvailable <= config.ROOM_TIER_0_ENERGY) {
                tier = config.ROOM_TIER_0_NAME;
            } else if (room.energyCapacityAvailable > config.ROOM_TIER_1_ENERGY && room.energyCapacityAvailable <= config.ROOM_TIER_2_ENERGY) {
                tier = config.ROOM_TIER_1_NAME;
            } else if (room.energyCapacityAvailable > config.ROOM_TIER_2_ENERGY && room.energyCapacityAvailable <= config.ROOM_TIER_3_ENERGY) {
                tier = config.ROOM_TIER_2_NAME;
            } else if (room.energyCapacityAvailable > config.ROOM_TIER_3_ENERGY && room.energyCapacityAvailable <= config.ROOM_TIER_4_ENERGY) {
                tier = config.ROOM_TIER_3_NAME;
            } else {
                tier = config.ROOM_TIER_4_NAME;
            }

            if (room.memory.tier !== tier) {
                var manifest = roleRunner[tier];
                if (!manifest) {
                    log.room(room, "unable to satisfy tier triggers");
                    return;
                }

                // spawn based on manifest
                room.memory.tier = tier;
                room.memory.manifest = manifest;
                log.room(room, "upgrading town to '" + tier + "'");
            }
        }
    };

    return runner.run();
};