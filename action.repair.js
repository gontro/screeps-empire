var config = require('engine.config');


module.exports = function(creep, job, target) {
    switch (target.structureType) {
        case STRUCTURE_ROAD:
            if (target.hits >= Math.floor(target.hitsMax * config.TRIGGER_REPAIR_ROAD)) {
                return ERR_INVALID_TARGET;
            }
            break;
        default:
            if (target.hits >= Math.floor(target.hitsMax * config.TRIGGER_REPAIR_BUILDING)) {
                return ERR_INVALID_TARGET;
            }
            break;
    }

    return creep.repair(target);
};