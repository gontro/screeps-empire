var config = require('engine.config');


module.exports = {
    room: function(room, msg, isError) {
        if (isError === true || config.DEBUG || config.DEBUG_CREEPS || room.memory.debug) {
            console.log('[ROOM:' + (isError ? 'ERROR:' : '') + room.name + '] ' + msg);
        }
    },

    creep: function(creep, msg, isError) {
        if (isError === true || config.DEBUG || config.DEBUG_CREEPS || creep.memory.debug) {
            console.log('[CREEP:' + (isError ? 'ERROR:' : '') + creep.name + '] ' + msg);
        }
    },

    cache: function(msg, isError) {
        if (isError === true || config.DEBUG || config.DEBUG_CACHE) {
            console.log('[CACHE' + (isError ? ':ERROR' : '') + '] ' + msg);
        }
    },

    debug: function(msg, isError) {
        if (isError === true || config.DEBUG) {
            console.log('[DEBUG' + (isError ? ':ERROR' : '') + '] ' + msg);
        }
    }
};