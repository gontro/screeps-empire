var config = require('engine.config');
var log = require('engine.log');


var ERROR_STRINGS = {
    '-1': 'ERR_NOT_OWNER',
    '-2': 'ERR_NO_PATH',
    '-3': 'ERR_NAME_EXISTS',
    '-4': 'ERR_BUSY',
    '-5': 'ERR_NOT_FOUND',
    '-6': 'ERR_NOT_ENOUGH_RESOURCES',
    '-7': 'ERR_INVALID_TARGET',
    '-8': 'ERR_FULL',
    '-9': 'ERR_NOT_IN_RANGE',
    '-10': 'ERR_INVALID_ARGS',
    '-11': 'ERR_TIRED',
    '-12': 'ERR_NO_BODYPART',
    '-14': 'ERR_RCL_NOT_ENOUGH',
    '-15': 'ERR_GCL_NOT_ENOUGH'
};


module.exports = function(creep, cache) {
    var runner = {
        initialize: function(force) {
            cache.add(creep, 'creep');

            if (!Game.rooms[creep.room.name]) {
                Game.rooms[creep.room.name] = creep.room;
            }

            if (!force && creep.memory.initialized) {
                return false;
            }

            creep.memory.initialized = true;
            creep.memory.debug = !!creep.memory.debug;
            creep.memory.role = creep.memory.role !== undefined ? creep.memory.role : '';
            creep.memory.model = creep.memory.model !== undefined ? creep.memory.model : '';
            creep.memory.currentJob = {};
            return true;
        },

        announceJob: function() {
            if (!creep.memory.currentJob.announced) {
                creep.memory.currentJob.announced = true;
                creep.say(creep.memory.currentJob.action);
            }
        },

        loadJob: function(job) {
            job = job || {};

            try {
                var lookupRunner = require(config.LOOKUP_PREFIX + job.lookup);
            } catch(e) {
                creep.say(config.CREEP_ERROR_SAY);
                log.creep(creep, "failure while trying to load lookup '" + config.LOOKUP_PREFIX + job.lookup + "'", true);
                return false;
            }

            var target = lookupRunner(creep);

            if (!target) {
                return false;
            }

            cache.add(target, job.lookup);

            var path = [];
            if (creep.room.name !== target.room.name) {
                path = creep.pos.findPathTo(target) || [];
                path.shift();
            }

            creep.memory.currentJob = {
                announced: false,
                action: job.action,
                lookup: job.lookup,
                resource: job.resource || null,
                target: {
                    id: target.id,
                    pos: target.pos,
                    path: path,
                    prefetch: !!job.prefetch
                }
            };

            log.creep(creep, "START " + job.action + " @ " + target.id);
            return true;
        },

        selectJob: function(jobs) {
            var self = this;

            if (creep.carry.energy === 0) {
                return this.loadJob({ action: 'harvest', lookup: 'localSource', prefetch: true });
            }

            var foundJob = false;
            _.forEach(jobs, function(job) {
                if (foundJob) {
                    return;
                }

                if (self.loadJob(job)) {
                    foundJob = job;
                }
            });

            return foundJob;
        },

        processJob: function() {
            // announce job to the world
            this.announceJob();

            // check target
            var target = cache.getByKey(creep.memory.currentJob.target.id) || null;
            if (!target) {
                target = Game.getObjectById(creep.memory.currentJob.target.id);
                cache.add(target, creep.memory.currentJob.lookup);

                if (!target) {
                    log.creep(creep, "error while targeting '" + creep.memory.currentJob.target.id + "'", true);
                    return ERR_INVALID_TARGET;
                }
            }

            var actionRunner;
            try {
                actionRunner = require(config.ACTION_PREFIX + creep.memory.currentJob.action);
            } catch(e) {
                creep.say(config.CREEP_ERROR_SAY);
                log.creep(creep, "failure while trying to load action '" + config.ACTION_PREFIX + creep.memory.currentJob.action + "'", true);
                return false;
            }

            var result = actionRunner(creep, creep.memory.currentJob, target);
            if (result === ERR_NOT_IN_RANGE) {
                try {
                    actionRunner = require(config.ACTION_PREFIX + config.MOVE_ACTION);
                } catch(e) {
                    creep.say(config.CREEP_ERROR_SAY);
                    log.creep(creep, "failure while trying to load action '" + config.ACTION_PREFIX + config.MOVE_ACTION + "'", true);
                    return false;
                }

                return actionRunner(creep, creep.memory.currentJob, target);
            }

            return result;
        },

        run: function() {
            this.initialize();

            if (creep.spawning) {
                return;
            }

            if (!creep.memory.role || !creep.memory.model) {
                creep.say(config.CREEP_IDLE_SAY);
                return;
            }

            if (!creep.memory.currentJob.action) {
                try {
                    var roleRunner = require(config.CREEP_ROLE_PREFIX + creep.memory.role);
                    var role = roleRunner[creep.memory.model];
                } catch(e) {
                    creep.say(config.CREEP_ERROR_SAY);
                    log.creep(creep, "failure while trying to load role '" + config.CREEP_ROLE_PREFIX + creep.memory.role + "'", true);
                    return;
                }

                if (!this.selectJob(role.jobs)) {
                    creep.say(config.CREEP_IDLE_SAY);
                    return;
                }
            }

            // run job
            var result = this.processJob();
            if (result !== OK && result !== ERR_TIRED) {

                if (result === ERR_NO_PATH) {
                    return;
                }

                var job = creep.memory.currentJob;
                creep.memory.currentJob = {};
                log.creep(creep, 'STOP ' + job.action + ':' + ERROR_STRINGS['' + result] + " @ " + job.target.id);
                return job;
            }
        }
    };

    return runner.run();
};