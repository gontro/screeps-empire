var roomRunner = require('engine.runner.room');
var spawnRunner = require('engine.runner.spawn');
var creepRunner = require('engine.runner.creep');


module.exports = function(cache) {
    var runner = {
        cleanMemory: function() {
            // Cleanup rooms
            for (var roomName in Memory.rooms) {
                if (!Game.rooms[roomName]) {
                    delete Memory.rooms[roomName];
                }
            }

            // Cleanup spawns
            for (var spawnName in Memory.spawns) {
                if (!Game.spawns[spawnName]) {
                    delete Memory.spawns[spawnName];
                }
            }

            // Cleanup creeps
            for (var creepName in Memory.creeps) {
                if (!Game.creeps[creepName]) {
                    delete Memory.creeps[creepName];
                }
            }
        },

        run: function() {
            // Remove corpses from memory
            this.cleanMemory();

            // Run ai for rooms
            for (var roomName in Game.rooms) {
                roomRunner(Game.rooms[roomName], cache);
            }

            // Run ai for spawns
            for (var spawnName in Game.spawns) {
                spawnRunner(Game.spawns[spawnName], cache);
            }

            // Run ai for creeps
            for (var creepName in Game.creeps) {
                creepRunner(Game.creeps[creepName], cache);
            }
        }
    };

    return runner.run();
};