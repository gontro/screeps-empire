var config = require('engine.config');
var log = require('engine.log');


module.exports = function(spawn, cache) {
    var runner = {

        initialize: function(force) {
            cache.add(spawn, 'spawn');

            if (!Game.rooms[spawn.room.name]) {
                Game.rooms[spawn.room.name] = spawn.room;
            }

            if (spawn.memory.initialized && !force) {
                return false;
            }

            spawn.memory.initialized = true;
            spawn.memory.debug = !!spawn.memory.debug;
            spawn.memory.role = spawn.memory.role !== undefined ? spawn.memory.role : '';
            spawn.memory.jobs = [];
            return true;
        },

        bodyCost: function(body) {
            return body.reduce(function(cost, part) {
                return cost + BODYPART_COST[part];
            }, 0);
        },

        run: function() {
            var self = this;

            // Ensure setup
            this.initialize();

            // keep spawning
            if (spawn.room.energyAvailable === spawn.room.energyCapacityAvailable &&
                spawn.room.memory.manifest && spawn.room.memory.manifest.length) {

                var spawningCreep = false;
                _.forEach(spawn.room.memory.manifest, function(entity) {
                    if (spawningCreep) {
                        return;
                    }

                    var currentCreeps = _.filter(Memory.creeps, { role: entity.role, model: entity.model });

                    if (currentCreeps.length < entity.amount) {
                        try {
                            var roleRunner = require(config.CREEP_ROLE_PREFIX + entity.role);
                            var role = roleRunner[entity.model];
                        } catch(e) {
                            log.spawn(spawn, "failure while trying to load role '" + config.CREEP_ROLE_PREFIX + entity.role + "'", true);
                            return;
                        }

                        if (self.bodyCost(role.body) <= spawn.room.energyAvailable) {
                            spawn.createCreep(role.body, null, { role: entity.role, model: entity.model });
                            spawningCreep = true;
                        }
                    }
                });
            }
        }
    };

    return runner.run();
};