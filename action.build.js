module.exports = function(creep, job, target) {
    if (target.energy === 0) {
        return ERR_NOT_ENOUGH_ENERGY;
    }

    return creep.build(target);
};