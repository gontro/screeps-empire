module.exports = {
    DEBUG: false,
    DEBUG_ROOMS: false,
    DEBUG_CREEPS: false,
    DEBUG_CACHE: false,

    MOVE_ACTION: 'move',

    TRIGGER_REPAIR_ROAD: 0.8,
    TRIGGER_REPAIR_BUILDING: 0.9,

    ROOM_STARTING_ROLE: 'village',
    ROOM_TIER_0_NAME: 'tiny',
    ROOM_TIER_0_ENERGY: 300,
    ROOM_TIER_1_NAME: 'small',
    ROOM_TIER_1_ENERGY: 500,
    ROOM_TIER_2_NAME: 'bustling',
    ROOM_TIER_2_ENERGY: 800,
    ROOM_TIER_3_NAME: 'large',
    ROOM_TIER_3_ENERGY: 1200,
    ROOM_TIER_4_NAME: 'massive',
    ROOM_TIER_4_ENERGY: 2000,

    ROOM_TICK_RATE: 10,
    SPAWN_TICK_RATE: 5,

    CREEP_ERROR_SAY: '#!@*$ERR!!',
    CREEP_IDLE_SAY: 'Idle..?',

    ACTION_PREFIX: 'action.',
    LOOKUP_PREFIX: 'lookup.',
    GAME_ROLE_PREFIX: 'role.game.',
    ROOM_ROLE_PREFIX: 'role.room.',
    SPAWN_ROLE_PREFIX: 'role.spawn.',
    CREEP_ROLE_PREFIX: 'role.creep.'
};