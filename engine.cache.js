var log = require('engine.log');


module.exports = {
    items: {},

    has: function(item) {
        item = item || {};
        var key = item.id || item.name || null;
        return key ? !!this.hasByKey(key) : false;
    },

    hasByKey: function(key) {
        return !!this.items[key];
    },

    getByKey: function(key) {
        return this.hasByKey(key) ? this.items[key] : false;
    },

    add: function(item, type) {
        item = item || {};
        var key = item.id || item.name || null;
        if (!key) {
            return false;
        }
        if (this.has(item)) {
            return false;
        }
        this.items[key] = item;
        if (item.room) {
            this.add(item.room, 'room');
        }
        if (item.controller) {
            this.add(item.room, 'controller');
        }
        log.cache((type ? type + ' @ ' : (item.structureType ? item.structureType + ' @ ' : '')) + key);
        return true;
    },

    remove: function(item) {
        item = item || {};
        var key = item.id || item.name || null;
        return this.removeByKey(key);
    },

    removeByKey: function(key) {
        if (!this.hasByKey(key)) {
            return false;
        }
        delete this.items[key];
        return true;
    }
};