var config = require('engine.config');


module.exports = function(creep) {
    return creep.pos.findClosestByRange(FIND_STRUCTURES, {
        filter: function (structure) {
            return structure.structureType === STRUCTURE_ROAD &&
                structure.hits < Math.floor(structure.hitsMax * config.TRIGGER_REPAIR_ROAD);
        }
    });
};