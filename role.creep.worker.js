module.exports = {
    small: {
        body: [MOVE, WORK, CARRY],
        jobs: [
            { action: 'recharge', lookup: 'buildingNeedsEnergy' },
            { action: 'repair', lookup: 'buildingNeedsRepair' },
            { action: 'build', lookup: 'needsConstruction' },
            { action: 'repair', lookup: 'roadNeedsRepair' },
            { action: 'upgrade', lookup: 'localController' }
        ]
    },
    medium: {
        body: [MOVE, MOVE, WORK, CARRY],
        jobs: [
            { action: 'recharge', lookup: 'buildingNeedsEnergy' },
            { action: 'repair', lookup: 'buildingNeedsRepair' },
            { action: 'build', lookup: 'needsConstruction' },
            { action: 'repair', lookup: 'roadNeedsRepair' },
            { action: 'upgrade', lookup: 'localController' }
        ]
    }
};