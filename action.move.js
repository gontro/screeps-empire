module.exports = function(creep, job, target) {
    if (target) {
        return creep.moveTo(target);
    } else if (job.target.path.length) {
        var nextMove = job.target.path[0];

        var result = creep.moveTo(creep.pos.x + nextMove.dx, creep.pos.y + nextMove.dy);
        if (result === OK) {
            job.target.path.shift();
        }

        return result;
    }

    return ERR_INVALID_TARGET;
};