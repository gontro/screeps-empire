module.exports = function(creep, job, target) {
    if (_.sum(creep.carry) >= creep.carryCapacity) {
        return ERR_FULL;
    }

    return creep.harvest(target);
};