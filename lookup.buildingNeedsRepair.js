var config = require('engine.config');


module.exports = function(creep) {
    return creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
        filter: function (structure) {
            return structure.hits < Math.floor(structure.hitsMax * config.TRIGGER_REPAIR_BUILDING);
        }
    });
};