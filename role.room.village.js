var config = require('engine.config');


var role = {};

role[config.ROOM_TIER_0_NAME] = [
    { role: 'worker', model: 'small', amount: 3 }
];

role[config.ROOM_TIER_1_NAME] = [
    { role: 'worker', model: 'small', amount: 3 },
    { role: 'worker', model: 'medium', amount: 2 }
];

role[config.ROOM_TIER_2_NAME] = [
    { role: 'worker', model: 'small', amount: 8 }
];

role[config.ROOM_TIER_3_NAME] = [
    { role: 'worker', model: 'small', amount: 10 }
];

role[config.ROOM_TIER_4_NAME] = [
    { role: 'worker', model: 'small', amount: 12 }
];

module.exports = role;